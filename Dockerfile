###########################
# Build executable binary #
###########################

FROM golang:1.16 AS builder

# Add Maintainer Info
LABEL maintainer="Tri Dao <minhtri1396@gmail.com>"

# Set the Current Working Directory inside the container
WORKDIR /app/

# Install protoc
ENV PROTOC_ZIP=protoc-3.14.0-linux-x86_64.zip
RUN curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.14.0/$PROTOC_ZIP
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install zip unzip
RUN unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
RUN unzip -o $PROTOC_ZIP -d /usr/local 'include/*'
RUN rm -f $PROTOC_ZIP
RUN go get google.golang.org/protobuf/cmd/protoc-gen-go
RUN go get google.golang.org/grpc/cmd/protoc-gen-go-grpc


# Copy the sources from the current directory to the Working Directory inside the container
COPY . .

# Build GRPC
RUN protoc --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --proto_path=./internal/handler/cdnfs --go_out=./internal/handler/cdnfs --go-grpc_out=./internal/handler/cdnfs ./internal/handler/cdnfs/fs_svc.proto

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod tidy
RUN go mod download

# Build
RUN CGO_ENABLED=0 GOOS=linux go build -tags=jsoniter -ldflags "-w -s" -a -installsuffix cgo -o fs-svc ./cmd


###############
# Build image #
###############

FROM gcr.io/distroless/base-debian10

WORKDIR /root/

# # Copy the Pre-built binary file from the previous stage
COPY --from=builder /app/fs-svc .

# Run the executable
CMD ["./fs-svc"]
