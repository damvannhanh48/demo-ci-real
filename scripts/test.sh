# Test log package
go test ./pkg/log/entity ./pkg/log/logger ./pkg/log

# Test cloudstorage package
go test ./pkg/cloudstorage

# Test shared
go test ./internal/shared/config ./internal/shared

# Test repository
go test ./internal/repository/repofs
