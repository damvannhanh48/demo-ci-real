# Remember to update Dockerfile after updating this file (build.sh)

protoc --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --proto_path=./internal/handler/cdnfs --go_out=./internal/handler/cdnfs --go-grpc_out=./internal/handler/cdnfs ./internal/handler/cdnfs/fs_svc.proto