package fscmd

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"filestorage-service/internal/core/port"
	"filestorage-service/internal/core/service"
	"filestorage-service/internal/handler/cdnfs"
	"filestorage-service/internal/repository/repofs"
	"filestorage-service/internal/shared"
	"filestorage-service/internal/shared/config"
	"filestorage-service/pkg/consul"
	"filestorage-service/pkg/database"
	"filestorage-service/pkg/log"
	logentity "filestorage-service/pkg/log/entity"
	"filestorage-service/pkg/log/logger"

	"github.com/spf13/cobra"
	"go.uber.org/dig"
)

var (
	container     *dig.Container
	env           string
	cfgFileName   string
	cfgFolderPath string
	exitServer    context.CancelFunc
)

var RootCmd = &cobra.Command{
	Use:   "fsv",
	Short: "File Storage Service",
	Run:   run,
}

func Execute() error {
	// RootCmd.AddCommand(subcmd.NewLOSServiceCmd())
	// RootCmd.AddCommand(subcmd.NewMigrateCommand())
	return RootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initialize)
	RootCmd.PersistentFlags().StringVar(&env, "env", "", "environment (default: local)")
	RootCmd.PersistentFlags().StringVar(&cfgFileName, "cfgFileName", "config.local", "config file name (default: config.local)")
	RootCmd.PersistentFlags().StringVar(&cfgFolderPath, "cfgFolderPath", "./config", "config path (default: ./config)")
}

func initialize() {
	container = buildContainer()
}

func run(cmd *cobra.Command, args []string) {
	if len(env) == 0 {
		env = os.Getenv("ENV")
		if len(env) == 0 {
			env = shared.LocalEnvironment.String()
		}
	}
	container.Invoke(func(ctx context.Context, cfg config.Config, logWriter log.Log, consulClient consul.ConsulClient, serverHandler cdnfs.GrpcHandler) {
		// Warm up
		logWriter.Info(logentity.LogItem{
			LogType: logentity.ConsoleLogType,
			Message: "warming up FileStorage server",
		})
		serverConfig := cfg.GetServerConfig()
		if len(serverConfig.Hostname) > 0 && serverConfig.Hostname != "dev" {
			err := consulClient.RegisterService(
				serverConfig.Hostname,
				serverConfig.Name,
				serverConfig.IP,
				serverConfig.Port,
			)
			if err != nil {
				logWriter.Fatal(logentity.LogItem{
					LogType: logentity.ConsoleLogType,
					Message: "register service failed",
					Error:   err,
				})
			}
		}

		// Start server
		logWriter.Info(logentity.LogItem{
			LogType: logentity.ConsoleLogType,
			Message: "starting FS server",
		})

		serverExited := make(chan bool)
		go func() {
			err := serverHandler.Begin(ctx)
			if err != nil {
				logWriter.Fatal(logentity.LogItem{
					LogType: logentity.ConsoleLogType,
					Message: "start FS server failed",
					Error:   err,
				})
			}
			close(serverExited)
		}()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
		<-sig
		logWriter.Info(logentity.LogItem{
			LogType: logentity.ConsoleLogType,
			Message: "received interrupt. Exit",
		})

		// Stop server
		exitServer()
		<-serverExited

		logWriter.Info(logentity.LogItem{
			LogType: logentity.ConsoleLogType,
			Message: "stopped FS server",
		})
	})
}

func buildContainer() *dig.Container {
	container := dig.New()

	// Setup context
	container.Provide(func() context.Context {
		var ctx context.Context
		ctx, exitServer = context.WithCancel(context.Background())
		return ctx
	})

	// Setup log
	container.Provide(func() log.Log {
		levelLog := logentity.DebugLevel
		if env == shared.ProdEnvironment.String() {
			levelLog = logentity.InfoLevel
		}
		logWriter, err := log.New([]logger.Logger{
			logger.NewConsoleLogger("", levelLog),
		})
		if err != nil {
			panic("setup log failed: " + err.Error())
		}
		return logWriter
	})

	// Setup config
	container.Provide(func() config.Config {
		if env != shared.LocalEnvironment.String() {
			return config.NewEnvConfig()
		}
		cfg, err := config.NewViperConfig(cfgFolderPath, cfgFileName)
		if err != nil {
			panic("setup config failed: " + err.Error())
		}
		return cfg
	})

	// Setup consul
	container.Provide(func(cfg config.Config) consul.ConsulClient {
		return consul.NewConsulClient(cfg.GetConsulConfig(), "key/ca-cert.pem")
	})

	// Setup FileStorage repository
	container.Provide(func(cfg config.Config) port.FileStorageRepository {
		db := database.NewDatabase(database.NewMysqlConnector())
		err := db.Connect(cfg.GetDBConfig())
		if err != nil {
			panic("setup FileStorage repository failed: " + err.Error())
		}
		return repofs.NewMysqlRepository(db)
	})

	// Setup FileStorage service
	container.Provide(func(cfg config.Config, serviceRepo port.FileStorageRepository) port.FileStorageService {
		return service.NewFileStorageService(cfg.GetCloudStorageConfig(), serviceRepo)
	})

	// Setup handler
	container.Provide(func(cfg config.Config, service port.FileStorageService) cdnfs.GrpcHandler {
		return cdnfs.New(shared.ParseEnv(env), cfg, service)
	})

	return container
}
