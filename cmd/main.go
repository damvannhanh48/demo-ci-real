package main

import (
	"log"

	"filestorage-service/cmd/fscmd"
)

func main() {
	if err := fscmd.Execute(); err != nil {
		log.Fatal("Execute program failed", err)
	}
}
