DELIMITER //


DROP PROCEDURE IF EXISTS fs_svc.validate_checksum //
CREATE PROCEDURE fs_svc.validate_checksum
(   
    IN param_project_id VARCHAR(64),
    IN param_file_type TINYINT,
    IN param_checksum VARCHAR(64),
    IN param_timestamp BIGINT
)
BEGIN
    IF NOT EXISTS (
		SELECT 1
        FROM fs_svc.project_checksum pc
        WHERE pc.project = param_project_id AND
            pc.`checksum` = param_checksum AND
            pc.`timestamp` = param_timestamp AND
            pc.valid = 1
        LIMIT 1
	) THEN
		SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 1001, MESSAGE_TEXT = 'INVALID CHECKSUM';
	END IF;
END //


DROP PROCEDURE IF EXISTS fs_svc.get_storage //
CREATE PROCEDURE fs_svc.get_storage
(   
    IN param_project_id VARCHAR(64),
    IN param_file_type TINYINT
)
BEGIN
    SELECT
        p.access_key,
        p.user_id,
        p.region_id,
        p.cloud_id,
        p.container
    FROM fs_svc.project p
    WHERE p.id = param_project_id AND
        p.file_type = param_file_type
    LIMIT 1;
END //

DROP PROCEDURE IF EXISTS fs_svc.add_file_url //
CREATE PROCEDURE fs_svc.add_file_url
(   
    IN param_project VARCHAR(64),
    IN param_file_type TINYINT,
    IN param_file_name NVARCHAR(150),
    IN param_file_url NVARCHAR(210),
    IN param_expired_timestamp INT
)
BEGIN
    INSERT INTO fs_svc.file_url
    (
        project,
        file_type,
        file_name,
        file_url,
        expired_timestamp
    )
    VALUES
    (
        param_project,
        param_file_type,
        param_file_name,
        param_file_url,
        param_expired_timestamp
    )
    ON DUPLICATE KEY UPDATE file_url = VALUES(file_url),
        expired_timestamp = VALUES(expired_timestamp);
END //


DROP PROCEDURE IF EXISTS fs_svc.get_file_url //
CREATE PROCEDURE fs_svc.get_file_url
(
    IN param_project VARCHAR(64),
    IN param_file_type TINYINT,
    IN param_file_name NVARCHAR(150)
)
BEGIN
    SELECT fu.file_url
    FROM fs_svc.file_url fu
    WHERE fu.project = param_project AND
        fu.file_type = param_file_type AND
        fu.file_name = param_file_name
    LIMIT 1;
END //


DROP PROCEDURE IF EXISTS fs_svc.delete_file_url //
CREATE PROCEDURE fs_svc.delete_file_url
(
    IN param_project VARCHAR(64),
    IN param_file_type TINYINT,
    IN param_file_name NVARCHAR(150)
)
BEGIN
    DELETE FROM fs_svc.file_url
    WHERE project = param_project AND
        file_type = param_file_type AND
        file_name = param_file_name
    LIMIT 1;
END //