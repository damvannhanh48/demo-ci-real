INSERT INTO `fs_svc`.`project`
(
    id,
    file_type,
    access_key,
    user_id,
    region_id,
    cloud_id,
    container
)
VALUES
(
    "project_id_123",
    1,
    "access_id_123",
    6158793,
    "region_id_123",
    "cloud_id_123",
    "container123"
);


INSERT INTO `fs_svc`.`project_checksum`
(   
    project,
    `checksum`,
    `timestamp`,
    valid
)
VALUES
(
    "project_id_123",
    "checksum_123",
    102456897,
    1
);