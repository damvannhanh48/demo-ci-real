CREATE SCHEMA IF NOT EXISTS `fs_svc` DEFAULT CHARACTER SET utf8;


CREATE TABLE IF NOT EXISTS `fs_svc`.`file_type` (
	id TINYINT NOT NULL,
    `name` NVARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS `fs_svc`.`project` (
	id VARCHAR(64) NOT NULL,
    file_type TINYINT NOT NULL COMMENT 'Link to a file type',
    access_key VARCHAR(36) COMMENT 'access_key on cloud storage',
    user_id INT COMMENT 'userId on cloud storage',
    region_id VARCHAR(36) COMMENT 'regionId on cloud storage',
    cloud_id VARCHAR(36) COMMENT 'projectId on cloud storage',
    container VARCHAR(50) COMMENT 'containerName on cloud storage',
    PRIMARY KEY (id, file_type),
    CONSTRAINT `fk_file_type_from_project`
		FOREIGN KEY (`file_type`)
		REFERENCES `fs_svc`.`file_type` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	INDEX `file_type_idx` (`file_type` ASC)
) ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS `fs_svc`.`project_checksum` (
	project VARCHAR(64) NOT NULL,
    `checksum` VARCHAR(64) NOT NULL,
    `timestamp` INT NOT NULL,
    valid BIT DEFAULT 1,
    PRIMARY KEY (project, `checksum`),
    CONSTRAINT `fk_project_from_project_checksum`
		FOREIGN KEY (`project`)
		REFERENCES `fs_svc`.`project` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	INDEX `project_idx` (`project` ASC)
) ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS `fs_svc`.`file_limit_size` (
	project VARCHAR(64) NOT NULL,
    file_type TINYINT NOT NULL COMMENT 'Link to a file type',
    limit_size INT DEFAULT 1, # MB
    PRIMARY KEY (project, file_type),
    CONSTRAINT `fk_project_from_file_limit_size`
		FOREIGN KEY (`project`)
		REFERENCES `fs_svc`.`project` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	INDEX `project_idx` (`project` ASC),
    CONSTRAINT `fk_file_type_from_file_limit_size`
		FOREIGN KEY (`file_type`)
		REFERENCES `fs_svc`.`file_type` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	INDEX `file_type_idx` (`file_type` ASC)
 ) ENGINE=INNODB;
 

CREATE TABLE IF NOT EXISTS `fs_svc`.`file_url` (
	project VARCHAR(64) NOT NULL,
    file_type TINYINT NOT NULL COMMENT 'Link to a file type',
    file_name NVARCHAR(150) COMMENT 'fileName on cloud storage',
    file_url NVARCHAR(210),
    expired_timestamp INT,
    PRIMARY KEY (project, file_type, file_name),
    CONSTRAINT `fk_project_from_file_url`
		FOREIGN KEY (`project`)
		REFERENCES `fs_svc`.`project` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	INDEX `project_idx` (`project` ASC),
    CONSTRAINT `fk_file_type_from_file_url`
		FOREIGN KEY (`file_type`)
		REFERENCES `fs_svc`.`file_type` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	INDEX `file_type_idx` (`file_type` ASC)
) ENGINE=INNODB;
