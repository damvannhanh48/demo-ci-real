module filestorage-service

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/go-sql-driver/mysql v1.6.0
	github.com/goldeneye-inside/consul-connector-golang v1.0.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/ory/dockertest/v3 v3.7.0
	github.com/rs/zerolog v1.23.0
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/dig v1.11.0
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.26.0
)
