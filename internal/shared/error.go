package shared

import "fmt"

type Error struct {
	Type    string `json:"type"`    // database_error | param_error | internal_error
	Code    string `json:"code"`    // a short string indicating the error code reported
	Message string `json:"message"` // more details about the error (human-readable)
}

func (p *Error) Error() string {
	return fmt.Sprintf("ErrorType: %v, ErrorCode %v, ErrorMessage: %v", p.Type, p.Code, p.Message)
}
