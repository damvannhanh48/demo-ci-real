package shared

// Constants of CONFIG
const (
	// MYSQL
	CONFIG_KEY_MYSQL_ADDRESS               = "mysql.ADDRESS"
	CONFIG_KEY_MYSQL_USERNAME              = "mysql.USER_NAME"
	CONFIG_KEY_MYSQL_PASSWORD              = "mysql.PASSWORD"
	CONFIG_KEY_MYSQL_DATABASE              = "mysql.DATABASE"
	CONFIG_KEY_MYSQL_NUMBER_MAX_CONNS      = "mysql.NUMBER_MAX_CONNS"
	CONFIG_KEY_MYSQL_NUMBER_IDLE_CONNS     = "mysql.NUMBER_IDLE_CONNS"
	CONFIG_KEY_ENV_MYSQL_ADDRESS           = "DATABASE_ADDRESS"
	CONFIG_KEY_ENV_MYSQL_USERNAME          = "DATABASE_USERNAME"
	CONFIG_KEY_ENV_MYSQL_PASSWORD          = "DATABASE_PASSWORD"
	CONFIG_KEY_ENV_MYSQL_DATABASE          = "DATABASE_NAME"
	CONFIG_KEY_ENV_MYSQL_NUMBER_MAX_CONNS  = "DATABASE_NUMBER_MAX_CONNS"
	CONFIG_KEY_ENV_MYSQL_NUMBER_IDLE_CONNS = "DATABASE_NUMBER_IDLE_CONNS"

	// GRPC SERVER
	CONFIG_KEY_GRPC_SERVER_HOSTNAME        = "grpcserver.HOSTNAME"
	CONFIG_KEY_GRPC_SERVER_NAME            = "grpcserver.NAME"
	CONFIG_KEY_GRPC_SERVER_IP              = "grpcserver.IP"
	CONFIG_KEY_GRPC_SERVER_PORT            = "grpcserver.PORT"
	CONFIG_KEY_GRPC_SERVER_PRIVATE_KEY     = "grpcserver.PRIVATE_KEY"
	CONFIG_KEY_GRPC_SERVER_CERTIFICATE     = "grpcserver.CERTIFICATE"
	CONFIG_KEY_ENV_GRPC_SERVER_HOSTNAME    = "SERVER_HOSTNAME"
	CONFIG_KEY_ENV_GRPC_SERVER_NAME        = "SERVER_NAME"
	CONFIG_KEY_ENV_GRPC_SERVER_IP          = "SERVER_IP"
	CONFIG_KEY_ENV_GRPC_SERVER_PORT        = "SERVER_PORT"
	CONFIG_KEY_ENV_GRPC_SERVER_PRIVATE_KEY = "SERVER_PRIVATE_KEY"
	CONFIG_KEY_ENV_GRPC_SERVER_CERTIFICATE = "SERVER_CERTIFICATE"

	// CONSUL
	CONFIG_KEY_CONSUL_TOKEN                 = "consul.TOKEN"
	CONFIG_KEY_CONSUL_ADDRESS               = "consul.ADDRESS"
	CONFIG_KEY_CONSUL_CERTIFICATE           = "consul.CERTIFICATE"
	CONFIG_KEY_CONSUL_HEALTH_CHECK_IP       = "consul.HEALTH_CHECK_IP"
	CONFIG_KEY_CONSUL_HEALTH_CHECK_PORT     = "consul.HEALTH_CHECK_PORT"
	CONFIG_KEY_ENV_CONSUL_TOKEN             = "CONSUL_TOKEN"
	CONFIG_KEY_ENV_CONSUL_ADDRESS           = "CONSUL_ADDRESS"
	CONFIG_KEY_ENV_CONSUL_CERTIFICATE       = "CONSUL_CERTIFICATE"
	CONFIG_KEY_ENV_CONSUL_HEALTH_CHECK_IP   = "CONSUL_HEALTH_CHECK_IP"
	CONFIG_KEY_ENV_CONSUL_HEALTH_CHECK_PORT = "CONSUL_HEALTH_CHECK_PORT"

	// FILE STORAGE
	CONFIG_KEY_CLOUD_STORAGE_LIMIT_SIZE     = "cloudstorage.LIMIT_SIZE"
	CONFIG_KEY_ENV_CLOUD_STORAGE_LIMIT_SIZE = "CLOUD_STORAGE_LIMIT_SIZE"
)
