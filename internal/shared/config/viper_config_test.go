package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestViperConfig(t *testing.T) {
	t.Run("error_cases", testViperConfigErrorCases)
	t.Run("success_cases", testViperConfigSuccessCases)
}

func testViperConfigErrorCases(t *testing.T) {
	_, err := NewViperConfig("config_folder_invalid", "config.test")
	assert.Error(t, err)

	_, err = NewViperConfig("config", "config.test.path_invalid")
	assert.Error(t, err)

	_, err = NewViperConfig("config_folder_invalid", "config.test.path_invalid")
	assert.Error(t, err)
}

func testViperConfigSuccessCases(t *testing.T) {
	cfg, err := NewViperConfig("../../../config", "config.test")
	assert.NoError(t, err)

	repoCfg := cfg.GetDBConfig()
	assert.Equal(t, repoCfg.Address, "localhost:3306")
	assert.Equal(t, repoCfg.UserName, "root")
	assert.Equal(t, repoCfg.Password, "root")
	assert.Equal(t, repoCfg.Database, "fs_svc")
	assert.Equal(t, repoCfg.NumberIdleConns, 2)
	assert.Equal(t, repoCfg.NumberMaxConns, 100)

	serverCfg := cfg.GetServerConfig()
	assert.Equal(t, serverCfg.Hostname, "server_hostname")
	assert.Equal(t, serverCfg.Name, "server_name")
	assert.Equal(t, serverCfg.IP, "server_ip")
	assert.Equal(t, serverCfg.Port, 8001)
	assert.Equal(t, serverCfg.PrivateKey, "server_privatekey")
	assert.Equal(t, serverCfg.Certificate, "server_certificate")

	consulCfg := cfg.GetConsulConfig()
	assert.Equal(t, consulCfg.Token, "consul_token")
	assert.Equal(t, consulCfg.Address, "consul_address")
	assert.Equal(t, consulCfg.Certificate, "consul_certificate")
	assert.Equal(t, consulCfg.HealthCheckIP, "consul_healthcheckip")
	assert.Equal(t, consulCfg.HealthCheckPort, 8002)

	cloudstorageCfg := cfg.GetCloudStorageConfig()
	assert.Equal(t, cloudstorageCfg.LimitSize, 1000000)

}
