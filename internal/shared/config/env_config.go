package config

import (
	"os"
	"strconv"

	"filestorage-service/internal/shared"
	"filestorage-service/pkg/cloudstorage"
	"filestorage-service/pkg/consul"
	"filestorage-service/pkg/database"
	"filestorage-service/pkg/grpcserver"
)

func NewEnvConfig() Config {
	return new(envConfig)
}

type envConfig struct {
}

func (p *envConfig) GetDBConfig() *database.Config {
	return &database.Config{
		UserName:        parseStringConfig(shared.CONFIG_KEY_ENV_MYSQL_USERNAME, "root"),
		Password:        parseStringConfig(shared.CONFIG_KEY_ENV_MYSQL_PASSWORD, "root"),
		Address:         parseStringConfig(shared.CONFIG_KEY_ENV_MYSQL_ADDRESS, "127.0.0.1:3306"),
		Database:        parseStringConfig(shared.CONFIG_KEY_ENV_MYSQL_DATABASE, "fs_svc"),
		NumberMaxConns:  int(parseNumberConfig(shared.CONFIG_KEY_ENV_MYSQL_NUMBER_MAX_CONNS, 10)),
		NumberIdleConns: int(parseNumberConfig(shared.CONFIG_KEY_ENV_MYSQL_NUMBER_IDLE_CONNS, 10)),
	}
}

func (p *envConfig) GetServerConfig() *grpcserver.Config {
	return &grpcserver.Config{
		Hostname:    parseStringConfig(shared.CONFIG_KEY_ENV_GRPC_SERVER_HOSTNAME, "dev"),
		Name:        parseStringConfig(shared.CONFIG_KEY_ENV_GRPC_SERVER_NAME, "cdn-fs-svc"),
		IP:          parseStringConfig(shared.CONFIG_KEY_ENV_GRPC_SERVER_IP, "127.0.0.1"),
		Port:        int(parseNumberConfig(shared.CONFIG_KEY_ENV_GRPC_SERVER_PORT, 8001)),
		PrivateKey:  parseStringConfig(shared.CONFIG_KEY_ENV_GRPC_SERVER_PRIVATE_KEY, ""),
		Certificate: parseStringConfig(shared.CONFIG_KEY_ENV_GRPC_SERVER_CERTIFICATE, ""),
	}
}

func (p *envConfig) GetConsulConfig() *consul.Config {
	return &consul.Config{
		Token:           parseStringConfig(shared.CONFIG_KEY_ENV_CONSUL_TOKEN, ""),
		Address:         parseStringConfig(shared.CONFIG_KEY_ENV_CONSUL_ADDRESS, ""),
		Certificate:     parseStringConfig(shared.CONFIG_KEY_ENV_CONSUL_CERTIFICATE, ""),
		HealthCheckIP:   parseStringConfig(shared.CONFIG_KEY_ENV_CONSUL_HEALTH_CHECK_IP, ""),
		HealthCheckPort: int(parseNumberConfig(shared.CONFIG_KEY_ENV_CONSUL_HEALTH_CHECK_PORT, 8002)),
	}
}

func (p *envConfig) GetCloudStorageConfig() *cloudstorage.Config {
	return &cloudstorage.Config{
		LimitSize: int(parseNumberConfig(shared.CONFIG_KEY_ENV_CLOUD_STORAGE_LIMIT_SIZE, 1000000)),
	}
}

func parseStringConfig(key, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}

func parseNumberConfig(key string, defaultValue int) int64 {
	value, err := strconv.ParseInt(os.Getenv(key), 10, 64)
	if err != nil {
		return int64(defaultValue)
	}
	return value
}
