package config

import (
	"filestorage-service/pkg/cloudstorage"
	"filestorage-service/pkg/consul"
	"filestorage-service/pkg/database"
	"filestorage-service/pkg/grpcserver"
)

type Config interface {
	GetDBConfig() *database.Config
	GetServerConfig() *grpcserver.Config
	GetConsulConfig() *consul.Config
	GetCloudStorageConfig() *cloudstorage.Config
}
