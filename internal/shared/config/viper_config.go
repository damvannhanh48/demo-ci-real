package config

import (
	"filestorage-service/internal/shared"
	"filestorage-service/pkg/cloudstorage"
	"filestorage-service/pkg/consul"
	"filestorage-service/pkg/database"
	"filestorage-service/pkg/grpcserver"

	"github.com/spf13/viper"
)

func NewViperConfig(cfgFolderPath, cfgFileName string) (Config, error) {
	p := &viperConfig{
		cfgFolderPath: cfgFolderPath,
		cfgFileName:   cfgFileName,
	}
	err := p.setup()
	return p, err
}

type viperConfig struct {
	cfgFolderPath string
	cfgFileName   string
}

func (p *viperConfig) setup() error {
	viper.SetConfigName(p.cfgFileName)
	viper.AddConfigPath(p.cfgFolderPath)
	viper.AutomaticEnv()

	// Read in config file
	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	// Watch config dynamically
	viper.WatchConfig()
	return nil
}

func (p *viperConfig) GetDBConfig() *database.Config {
	return &database.Config{
		UserName:        viper.GetString(shared.CONFIG_KEY_MYSQL_USERNAME),
		Password:        viper.GetString(shared.CONFIG_KEY_MYSQL_PASSWORD),
		Address:         viper.GetString(shared.CONFIG_KEY_MYSQL_ADDRESS),
		Database:        viper.GetString(shared.CONFIG_KEY_MYSQL_DATABASE),
		NumberMaxConns:  viper.GetInt(shared.CONFIG_KEY_MYSQL_NUMBER_MAX_CONNS),
		NumberIdleConns: viper.GetInt(shared.CONFIG_KEY_MYSQL_NUMBER_IDLE_CONNS),
	}
}

func (p *viperConfig) GetServerConfig() *grpcserver.Config {
	return &grpcserver.Config{
		Hostname:    viper.GetString(shared.CONFIG_KEY_GRPC_SERVER_HOSTNAME),
		Name:        viper.GetString(shared.CONFIG_KEY_GRPC_SERVER_NAME),
		IP:          viper.GetString(shared.CONFIG_KEY_GRPC_SERVER_IP),
		Port:        viper.GetInt(shared.CONFIG_KEY_GRPC_SERVER_PORT),
		PrivateKey:  viper.GetString(shared.CONFIG_KEY_GRPC_SERVER_PRIVATE_KEY),
		Certificate: viper.GetString(shared.CONFIG_KEY_GRPC_SERVER_CERTIFICATE),
	}
}

func (p *viperConfig) GetConsulConfig() *consul.Config {
	return &consul.Config{
		Token:           viper.GetString(shared.CONFIG_KEY_CONSUL_TOKEN),
		Address:         viper.GetString(shared.CONFIG_KEY_CONSUL_ADDRESS),
		Certificate:     viper.GetString(shared.CONFIG_KEY_CONSUL_CERTIFICATE),
		HealthCheckIP:   viper.GetString(shared.CONFIG_KEY_CONSUL_HEALTH_CHECK_IP),
		HealthCheckPort: viper.GetInt(shared.CONFIG_KEY_CONSUL_HEALTH_CHECK_PORT),
	}
}

func (p *viperConfig) GetCloudStorageConfig() *cloudstorage.Config {
	return &cloudstorage.Config{
		LimitSize: viper.GetInt(shared.CONFIG_KEY_CLOUD_STORAGE_LIMIT_SIZE),
	}
}
