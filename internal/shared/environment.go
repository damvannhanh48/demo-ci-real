package shared

type Environment int

const (
	DevEnvironment Environment = iota
	LocalEnvironment
	ProdEnvironment
)

func ParseEnv(env string) Environment {
	if env == "prod" {
		return ProdEnvironment
	}
	if env == "dev" {
		return DevEnvironment
	}
	return LocalEnvironment
}

func (p Environment) String() string {
	if p == ProdEnvironment {
		return "prod"
	}
	if p == DevEnvironment {
		return "dev"
	}
	return "local"
}
