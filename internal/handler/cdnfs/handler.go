package cdnfs

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"strconv"

	"filestorage-service/internal/core/domain"
	"filestorage-service/internal/core/port"
	"filestorage-service/internal/shared"
	"filestorage-service/internal/shared/config"

	"github.com/go-sql-driver/mysql"
	grpc "google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

type GrpcHandler interface {
	Begin(ctx context.Context) error
}

func New(env shared.Environment, cfg config.Config, fileStorageService port.FileStorageService) GrpcHandler {
	return &grpcHandler{
		env:                env,
		cfg:                cfg,
		fileStorageService: fileStorageService,
	}
}

type grpcHandler struct {
	UnimplementedFSServiceServer
	fileStorageService port.FileStorageService
	cfg                config.Config
	env                shared.Environment
}

func (p *grpcHandler) Begin(ctx context.Context) error {
	var (
		err  error
		cert tls.Certificate
		opts []grpc.ServerOption
	)
	cfgServer := p.cfg.GetServerConfig()

	if p.env != shared.LocalEnvironment {
		// Load Certificate
		privateKey := cfgServer.PrivateKey
		certificate := cfgServer.Certificate

		if len(certificate) == 0 {
			cert, err = tls.LoadX509KeyPair("key/server-cert.pem", "key/server-key.pem")
		} else {
			cert, err = tls.X509KeyPair([]byte(certificate), []byte(privateKey))
		}
		if err != nil {
			return err
		}

		opts = []grpc.ServerOption{
			grpc.Creds(credentials.NewServerTLSFromCert(&cert)),
		}
	}

	//Open port
	address := fmt.Sprintf("0.0.0.0:%d", cfgServer.Port)
	listen, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	// Start service
	server := grpc.NewServer(opts...)
	RegisterFSServiceServer(server, p)
	reflection.Register(server)

	go func() {
		<-ctx.Done()
		server.GracefulStop()
	}()
	return server.Serve(listen)
}

func (p *grpcHandler) buildSuccessReturnCode() *FSReturncode {
	return &FSReturncode{
		Service: p.cfg.GetServerConfig().Name,
		Result:  "SUCCESS",
		Error:   nil,
	}
}

func (p *grpcHandler) convertReturncode(err error) *FSReturncode {
	sqlErr, ok := err.(*mysql.MySQLError)
	if ok {
		return &FSReturncode{
			Service: p.cfg.GetServerConfig().Name,
			Result:  "FAIL",
			Error: &FSError{
				Type:    "database_error",
				Code:    strconv.Itoa(int(sqlErr.Number)),
				Message: sqlErr.Message,
			},
		}
	}
	customErr, ok := err.(*shared.Error)
	if !ok {
		return &FSReturncode{
			Service: p.cfg.GetServerConfig().Name,
			Result:  "FAIL",
			Error: &FSError{
				Type:    "internal_error",
				Code:    "-1",
				Message: err.Error(),
			},
		}
	}
	return &FSReturncode{
		Service: p.cfg.GetServerConfig().Name,
		Result:  "FAIL",
		Error: &FSError{
			Type:    customErr.Type,
			Code:    customErr.Code,
			Message: customErr.Message,
		},
	}
}

func (p *grpcHandler) UploadFile(ctx context.Context, req *ReqUploadFile) (*FSReturncode, error) {
	err := p.fileStorageService.Validate(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return p.convertReturncode(err), nil
	}

	err = p.fileStorageService.UploadFile(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		req.FilePath,
		req.FileName,
		req.FileContent,
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return p.convertReturncode(err), nil
	}

	return p.buildSuccessReturnCode(), nil
}

func (p *grpcHandler) GetFileTempUrl(ctx context.Context, req *ReqGetFileTempUrl) (*ResqGetFileTempUrl, error) {
	err := p.fileStorageService.Validate(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return &ResqGetFileTempUrl{
			ReturnCode: p.convertReturncode(err),
		}, nil
	}

	url, err := p.fileStorageService.GetFileTempUrl(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		req.FileName,
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return &ResqGetFileTempUrl{
			ReturnCode: p.convertReturncode(err),
		}, nil
	}

	return &ResqGetFileTempUrl{
		ReturnCode: p.buildSuccessReturnCode(),
		Url:        url,
	}, nil
}

func (p *grpcHandler) GetFileUrl(ctx context.Context, req *ReqGetFileUrl) (*ResqGetFileUrl, error) {
	err := p.fileStorageService.Validate(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return &ResqGetFileUrl{
			ReturnCode: p.convertReturncode(err),
		}, nil
	}

	url, err := p.fileStorageService.GetFileUrl(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		req.FileName,
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return &ResqGetFileUrl{
			ReturnCode: p.convertReturncode(err),
		}, nil
	}

	return &ResqGetFileUrl{
		ReturnCode: p.buildSuccessReturnCode(),
		Url:        url,
	}, nil
}

func (p *grpcHandler) DeleteFile(ctx context.Context, req *ReqDeleteFile) (*FSReturncode, error) {
	err := p.fileStorageService.Validate(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return p.convertReturncode(err), nil
	}

	err = p.fileStorageService.DeleteFile(
		ctx,
		req.ProjectId,
		int8(req.FileType),
		req.FilePath,
		req.FileName,
		&domain.Checksum{
			Value:     req.Checksum.Value,
			Timestamp: req.Checksum.Timestamp,
		},
	)
	if err != nil {
		return p.convertReturncode(err), nil
	}
	return p.buildSuccessReturnCode(), nil
}
