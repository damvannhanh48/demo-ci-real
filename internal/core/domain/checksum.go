package domain

type Checksum struct {
	Value     string
	Timestamp int64
}
