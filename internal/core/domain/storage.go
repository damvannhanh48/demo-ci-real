package domain

type Storage struct {
	AccessKey     string
	UserId        int
	RegionId      string
	CloudId       string
	ContainerName string
}
