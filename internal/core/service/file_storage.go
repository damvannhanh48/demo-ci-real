package service

import (
	"bytes"
	"context"
	"errors"
	"filestorage-service/internal/core/domain"
	"filestorage-service/internal/core/port"
	"filestorage-service/pkg/cloudstorage"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

func NewFileStorageService(cfg *cloudstorage.Config, fileStorageRepo port.FileStorageRepository) port.FileStorageService {
	return &fileStorageService{
		cfg:             cfg,
		fileStorageRepo: fileStorageRepo,
	}
}

type fileStorageService struct {
	cfg             *cloudstorage.Config
	fileStorageRepo port.FileStorageRepository
}

func (p *fileStorageService) getNewFileTempUrl(ctx context.Context, projectId string, fileType int8, fileName string, checksum *domain.Checksum) (string, error) {
	storage, err := p.fileStorageRepo.GetStorage(ctx, projectId, fileType)
	if err != nil {
		return "", err
	}

	fileStorage := cloudstorage.NewFileStorage(
		storage.UserId,
		storage.RegionId,
		storage.CloudId,
		storage.AccessKey,
		checksum.Value,
		checksum.Timestamp,
	)
	urls, err := fileStorage.GetFileTempUrls(storage.ContainerName, []string{fileName}, 3600)
	if err != nil {
		return "", err
	}
	fileUrl, ok := urls[fileName]
	if !ok {
		return "", errors.New("invalid file")
	}

	// check ulr existed
	resp, err := http.Head(fileUrl)
	if err != nil {
		return "", err
	}
	if resp.StatusCode == http.StatusNotFound {
		return "", nil
	}

	u, err := url.Parse(fileUrl)
	if err != nil {
		return "", err
	}
	expiredTimestampStr := u.Query().Get("temp_url_expires")
	var expiredTimestamp int64
	expiredTimestamp, err = strconv.ParseInt(expiredTimestampStr, 10, 32)
	if err != nil {
		return "", err
	}

	err = p.fileStorageRepo.AddFileUrl(ctx, projectId, fileType, fileName, fileUrl, expiredTimestamp)
	if err != nil {
		return "", err
	}

	return fileUrl, nil
}

func (p *fileStorageService) Validate(ctx context.Context, projectId string, fileType int8, checksum *domain.Checksum) error {
	return p.fileStorageRepo.Validate(ctx, projectId, fileType, checksum)
}

func (p *fileStorageService) UploadFile(
	ctx context.Context,
	projectId string,
	fileType int8,
	filePath,
	fileName string,
	fileContent []byte,
	checksum *domain.Checksum,
) error {
	storage, err := p.fileStorageRepo.GetStorage(ctx, projectId, fileType)
	if err != nil {
		return err
	}

	fileStorage := cloudstorage.NewFileStorage(
		storage.UserId,
		storage.RegionId,
		storage.CloudId,
		storage.AccessKey,
		checksum.Value,
		checksum.Timestamp,
	)

	fileContent, err = fileStorage.ResizeFile(fileContent, p.cfg.LimitSize)
	if err != nil {
		return err
	}

	var urlStr string
	urlStr, err = fileStorage.RegisterTempUrlForPostFile(storage.ContainerName, filePath, fileName, 3600)
	if err != nil {
		return err
	}

	err = fileStorage.PostFile(urlStr, bytes.NewBuffer(fileContent))
	if err != nil {
		return err
	}

	// update file
	fileName = filePath + "/" + fileName
	urls, err := fileStorage.GetFileTempUrls(storage.ContainerName, []string{fileName}, 3600)
	if err != nil {
		return err
	}
	fileUrl, ok := urls[fileName]
	if !ok {
		return errors.New("invalid file")
	}

	// check ulr existed
	resp, err := http.Head(fileUrl)
	if err != nil {
		return err
	}
	if resp.StatusCode == http.StatusNotFound {
		return nil
	}

	u, err := url.Parse(fileUrl)
	if err != nil {
		return err
	}
	expiredTimestampStr := u.Query().Get("temp_url_expires")
	var expiredTimestamp int64
	expiredTimestamp, err = strconv.ParseInt(expiredTimestampStr, 10, 32)
	if err != nil {
		return err
	}

	return p.fileStorageRepo.AddFileUrl(ctx, projectId, fileType, fileName, fileUrl, expiredTimestamp)
}

func (p *fileStorageService) GetFileTempUrl(ctx context.Context, projectId string, fileType int8, fileName string, checksum *domain.Checksum) (string, error) {
	fileUrl, err := p.fileStorageRepo.GetFileUrl(ctx, projectId, fileType, fileName)
	if err != nil {
		if err.Error() != "invalid project" {
			return "", err
		}
		return p.getNewFileTempUrl(ctx, projectId, fileType, fileName, checksum)
	}

	u, err := url.Parse(fileUrl)
	if err != nil {
		return "", err
	}
	expiredTimestampStr := u.Query().Get("temp_url_expires")
	var expiredTimestamp int64
	expiredTimestamp, err = strconv.ParseInt(expiredTimestampStr, 10, 32)
	if err != nil {
		return "", err
	}

	if (expiredTimestamp - 2) < time.Now().Unix() {
		return p.getNewFileTempUrl(ctx, projectId, fileType, fileName, checksum)
	}
	return fileUrl, nil
}

func (p *fileStorageService) GetFileTempUrls(
	ctx context.Context,
	projectId string,
	fileType int8,
	fileNames []string,
	checksum *domain.Checksum,
) (map[string]string, error) {
	storage, err := p.fileStorageRepo.GetStorage(ctx, projectId, fileType)
	if err != nil {
		return nil, err
	}
	fileStorage := cloudstorage.NewFileStorage(
		storage.UserId,
		storage.RegionId,
		storage.CloudId,
		storage.AccessKey,
		checksum.Value,
		checksum.Timestamp,
	)
	return fileStorage.GetFileTempUrls(storage.ContainerName, fileNames, 3600)
}

func (p *fileStorageService) GetFileUrl(
	ctx context.Context,
	projectId string,
	fileType int8,
	fileName string,
	checksum *domain.Checksum,
) (string, error) {
	storage, err := p.fileStorageRepo.GetStorage(ctx, projectId, fileType)
	if err != nil {
		return "", err
	}
	fileStorage := cloudstorage.NewFileStorage(
		storage.UserId,
		storage.RegionId,
		storage.CloudId,
		storage.AccessKey,
		checksum.Value,
		checksum.Timestamp,
	)
	return fileStorage.GetFileUrl(storage.CloudId, storage.ContainerName, fileName)
}

func (p *fileStorageService) DeleteFile(ctx context.Context, projectId string, fileType int8, filePath, fileName string, checksum *domain.Checksum) error {
	storage, err := p.fileStorageRepo.GetStorage(ctx, projectId, fileType)
	if err != nil {
		return err
	}

	fileStorage := cloudstorage.NewFileStorage(
		storage.UserId,
		storage.RegionId,
		storage.CloudId,
		storage.AccessKey,
		checksum.Value,
		checksum.Timestamp,
	)

	err = fileStorage.DeleteFile(storage.ContainerName, filePath, fileName)
	if err != nil {
		return err
	}

	return p.fileStorageRepo.DeleteFileUrl(
		ctx,
		projectId,
		fileType,
		filePath,
		fileName,
	)
}
