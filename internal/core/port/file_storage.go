package port

import (
	"context"
	"filestorage-service/internal/core/domain"
)

type FileStorageRepository interface {
	Validate(ctx context.Context, projectId string, fileType int8, checksum *domain.Checksum) error
	GetStorage(ctx context.Context, projectId string, fileType int8) (*domain.Storage, error)
	AddFileUrl(ctx context.Context, projectId string, fileType int8, fileName, fileUrl string, expiredTimestamp int64) error
	GetFileUrl(ctx context.Context, projectId string, fileType int8, fileName string) (string, error)
	DeleteFileUrl(ctx context.Context, projectId string, fileType int8, filePath, fileName string) error
}

type FileStorageService interface {
	Validate(ctx context.Context, projectId string, fileType int8, checksum *domain.Checksum) error
	UploadFile(ctx context.Context, projectId string, fileType int8, filePath, fileName string, fileContent []byte, checksum *domain.Checksum) error
	GetFileTempUrl(ctx context.Context, projectId string, fileType int8, fileName string, checksum *domain.Checksum) (string, error)
	GetFileTempUrls(ctx context.Context, projectId string, fileType int8, fileName []string, checksum *domain.Checksum) (map[string]string, error)
	GetFileUrl(ctx context.Context, projectId string, fileType int8, fileName string, checksum *domain.Checksum) (string, error)
	DeleteFile(ctx context.Context, projectId string, fileType int8, filePath, fileName string, checksum *domain.Checksum) error
}
