package repofs

import (
	"context"
	"filestorage-service/internal/core/domain"
	"filestorage-service/internal/core/port"
	"filestorage-service/internal/shared/config"
	"filestorage-service/pkg/database"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
)

func TestIntegrationRepoProduct(t *testing.T) {
	cfg, err := config.NewViperConfig("./../../../config", "config.test")
	assert.NoError(t, err)
	db := database.NewDatabase(database.NewDockerConnector([]string{
		"./../../../sql/tables.sql",
		"./../../../sql/default_data.sql",
		"./../../../sql/default_data_test.sql",
		"./../../../sql/procedures.sql",
	}))
	err = db.Connect(cfg.GetDBConfig())
	assert.NoError(t, err)

	repo := NewMysqlRepository(db)

	testValidateChecksumInvalidIntegration(t, repo)
	testValidateChecksumSuccessIntegration(t, repo)

	testGetStorageNotExistedIntegration(t, repo)
	testGetStorageIntegration(t, repo)

	testAddFileUrlUnSuccessIntegration(t, repo)
	testAddFileUrlSuccessIntegration(t, repo)

	testGetFileUrlNotExistedIntegration(t, repo)
	testGetFileUrlIntegration(t, repo)

	testDeleteFileUrlSuccessIntegration(t, repo)

	err = db.Disconnect()
	assert.NoError(t, err)
}

func testValidateChecksumInvalidIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_456"
	fileType := int8(1)
	checksum := &domain.Checksum{
		Value:     "checksum_456",
		Timestamp: 102456897,
	}

	err := repo.Validate(
		context.Background(),
		projectId,
		fileType,
		checksum,
	)
	assert.Error(t, err)
}

func testValidateChecksumSuccessIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_123"
	fileType := int8(1)
	checksum := &domain.Checksum{
		Value:     "checksum_123",
		Timestamp: 102456897,
	}

	err := repo.Validate(
		context.Background(),
		projectId,
		fileType,
		checksum,
	)
	assert.NoError(t, err)
}

func testGetStorageNotExistedIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_456"
	fileType := int8(1)

	storage, err := repo.GetStorage(
		context.Background(),
		projectId,
		fileType,
	)
	assert.Error(t, err)
	assert.Nil(t, storage)
}

func testGetStorageIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_123"
	fileType := int8(1)

	storage, err := repo.GetStorage(
		context.Background(),
		projectId,
		fileType,
	)
	assert.NoError(t, err)
	assert.NotNil(t, storage)
	assert.Equal(t, storage.AccessKey, "access_id_123")
	assert.Equal(t, storage.UserId, 6158793)
	assert.Equal(t, storage.RegionId, "region_id_123")
	assert.Equal(t, storage.CloudId, "cloud_id_123")
	assert.Equal(t, storage.ContainerName, "container123")
}

func testAddFileUrlUnSuccessIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_456"
	fileType := int8(1)
	fileName := "file_name_123"
	fileUrl := "file_url_123"
	expiredTimestamp := int64(123)

	err := repo.AddFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
		fileUrl,
		expiredTimestamp,
	)
	assert.Error(t, err)
}

func testAddFileUrlSuccessIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_123"
	fileType := int8(1)
	fileName := "file_name_123"
	fileUrl := "file_url_123"
	expiredTimestamp := int64(123)

	err := repo.AddFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
		fileUrl,
		expiredTimestamp,
	)
	assert.NoError(t, err)
}

func testGetFileUrlNotExistedIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_456"
	fileType := int8(1)
	fileName := "file_name_123"

	fileUrl, err := repo.GetFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
	)
	assert.Error(t, err)
	assert.Equal(t, fileUrl, "")
}

func testGetFileUrlIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_123"
	fileType := int8(1)
	fileName := "file_name_123"

	fileUrl, err := repo.GetFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
	)
	assert.NoError(t, err)
	assert.NotEqual(t, fileUrl, "")
}

func testDeleteFileUrlSuccessIntegration(t *testing.T, repo port.FileStorageRepository) {
	projectId := "project_id_123"
	fileType := int8(1)
	fileName := "file_name_123"
	filePath := "form"

	err := repo.DeleteFileUrl(
		context.Background(),
		projectId,
		fileType,
		filePath,
		fileName,
	)
	assert.NoError(t, err)
}
