package repofs

import (
	"context"
	"errors"

	"filestorage-service/internal/core/domain"
	"filestorage-service/internal/core/port"
	"filestorage-service/pkg/database"
)

type mysqlRepository struct {
	db database.Database
}

func NewMysqlRepository(db database.Database) port.FileStorageRepository {
	return &mysqlRepository{
		db: db,
	}
}

func (p *mysqlRepository) Validate(ctx context.Context, projectId string, fileType int8, checksum *domain.Checksum) error {
	return p.db.Transaction(ctx, func(ctx context.Context, conn database.Connection) error {
		_, err := conn.ExecContext(
			ctx,
			"call fs_svc.validate_checksum(?, ?, ?, ?)",
			projectId,
			fileType,
			checksum.Value,
			checksum.Timestamp,
		)
		return err
	})
}

func (p *mysqlRepository) GetStorage(ctx context.Context, projectId string, fileType int8) (*domain.Storage, error) {
	var storage *domain.Storage
	err := p.db.Transaction(ctx, func(ctx context.Context, conn database.Connection) error {
		rows, err := conn.QueryContext(
			ctx,
			"call fs_svc.get_storage(?, ?)",
			projectId,
			fileType,
		)
		if err != nil {
			return err
		}
		defer rows.Close()

		if !rows.Next() {
			return errors.New("invalid project")
		}

		storage = new(domain.Storage)
		return rows.Scan(
			&storage.AccessKey,
			&storage.UserId,
			&storage.RegionId,
			&storage.CloudId,
			&storage.ContainerName,
		)
	})
	return storage, err
}

func (p *mysqlRepository) AddFileUrl(ctx context.Context, projectId string, fileType int8, fileName, fileUrl string, expiredTimestamp int64) error {
	return p.db.Transaction(ctx, func(ctx context.Context, conn database.Connection) error {
		_, err := conn.ExecContext(
			ctx,
			"call fs_svc.add_file_url(?, ?, ?, ?, ?)",
			projectId,
			fileType,
			fileName,
			fileUrl,
			expiredTimestamp,
		)
		return err
	})
}

func (p *mysqlRepository) GetFileUrl(ctx context.Context, projectId string, fileType int8, fileName string) (string, error) {
	var fileUrl string
	err := p.db.Transaction(ctx, func(ctx context.Context, conn database.Connection) error {
		rows, err := conn.QueryContext(
			ctx,
			"call fs_svc.get_file_url(?, ?, ?)",
			projectId,
			fileType,
			fileName,
		)
		if err != nil {
			return err
		}
		defer rows.Close()

		if !rows.Next() {
			return errors.New("invalid project")
		}
		return rows.Scan(&fileUrl)
	})

	return fileUrl, err
}

func (p *mysqlRepository) DeleteFileUrl(ctx context.Context, projectId string, fileType int8, filePath, fileName string) error {
	return p.db.Transaction(ctx, func(ctx context.Context, conn database.Connection) error {
		fileName = filePath + "/" + fileName
		_, err := conn.ExecContext(
			ctx,
			"call fs_svc.delete_file_url(?, ?, ?)",
			projectId,
			fileType,
			fileName,
		)
		return err
	})
}
