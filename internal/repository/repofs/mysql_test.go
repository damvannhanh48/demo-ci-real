package repofs

import (
	"context"
	"errors"
	"testing"

	"filestorage-service/internal/core/domain"
	"filestorage-service/internal/core/port"
	"filestorage-service/internal/shared/config"
	"filestorage-service/pkg/database"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestFilestorageMysqlRepository(t *testing.T) {
	cfg, err := config.NewViperConfig("../../../config", "config.test")
	assert.NoError(t, err, "setup config failed")

	connector, mock, err := database.NewSqlMockConnector()
	assert.NoError(t, err, "init database connector failed")
	defer connector.Close()

	db := database.NewDatabase(connector)
	err = db.Connect(cfg.GetDBConfig())
	assert.NoError(t, err, "connect database failed")
	defer db.Disconnect()

	repo := NewMysqlRepository(db)

	t.Run("validate_checksum", func(t *testing.T) {
		testValidateChecksumInvalid(t, mock, repo)
		testValidateChecksumSuccess(t, mock, repo)
	})

	t.Run("get_storage", func(t *testing.T) {
		testGetStorage(t, mock, repo)
		testGetStorageNotExisted(t, mock, repo)
	})

	t.Run("add_file_url", func(t *testing.T) {
		testAddFileUrlUnSuccess(t, mock, repo)
		testAddFileUrlSuccess(t, mock, repo)
	})

	t.Run("get_file_url", func(t *testing.T) {
		testGetFileUrlNotExisted(t, mock, repo)
		testGetFileUrl(t, mock, repo)
	})

	t.Run("delete_file_url", func(t *testing.T) {
		testDeleteFileUrlUnSuccess(t, mock, repo)
		testDeleteFileUrlSuccess(t, mock, repo)
	})
}

func testValidateChecksumInvalid(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	checksum := &domain.Checksum{
		Value:     "checksum_value",
		Timestamp: 102456897,
	}

	mock.ExpectBegin()
	mock.ExpectExec("call fs_svc.validate_checksum").
		WithArgs(projectId, fileType, checksum.Value, checksum.Timestamp).
		WillReturnError(errors.New("invalid checksum")).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectRollback()

	err := repo.Validate(
		context.Background(),
		projectId,
		fileType,
		checksum,
	)
	assert.Error(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testValidateChecksumSuccess(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	checksum := &domain.Checksum{
		Value:     "checksum_value",
		Timestamp: 102456897,
	}

	mock.ExpectBegin()
	mock.ExpectExec("call fs_svc.validate_checksum").
		WithArgs(projectId, fileType, checksum.Value, checksum.Timestamp).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err := repo.Validate(
		context.Background(),
		projectId,
		fileType,
		checksum,
	)
	assert.NoError(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testGetStorageNotExisted(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)

	rows := mock.NewRows([]string{"AccessKey", "UserId", "RegionId", "CloudId", "ContainerName"})
	mock.ExpectBegin()
	mock.ExpectQuery("call fs_svc.get_storage").
		WithArgs(projectId, fileType).
		WillReturnRows(rows)
	mock.ExpectRollback()

	storage, err := repo.GetStorage(
		context.Background(),
		projectId,
		fileType,
	)
	assert.Error(t, err)
	assert.Nil(t, storage)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testGetStorage(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)

	rows := mock.NewRows([]string{"AccessKey", "UserId", "RegionId", "CloudId", "ContainerName"}).
		AddRow("access_key_value", 6158793, "region_id_value", "cloud_id_value", "container_name_value")
	mock.ExpectBegin()
	mock.ExpectQuery("call fs_svc.get_storage").
		WithArgs(projectId, fileType).
		WillReturnRows(rows)
	mock.ExpectCommit()

	storage, err := repo.GetStorage(
		context.Background(),
		projectId,
		fileType,
	)
	assert.NoError(t, err)
	assert.NotNil(t, storage)
	assert.Equal(t, storage.AccessKey, "access_key_value")
	assert.Equal(t, storage.UserId, 6158793)
	assert.Equal(t, storage.RegionId, "region_id_value")
	assert.Equal(t, storage.CloudId, "cloud_id_value")
	assert.Equal(t, storage.ContainerName, "container_name_value")

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testAddFileUrlUnSuccess(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	fileName := "file_name"
	fileUrl := "file_url"
	expiredTimestamp := int64(123)

	mock.ExpectBegin()
	mock.ExpectExec("call fs_svc.add_file_url").
		WithArgs(projectId, fileType, fileName, fileUrl, expiredTimestamp).
		WillReturnError(errors.New("invalid project")).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectRollback()

	err := repo.AddFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
		fileUrl,
		expiredTimestamp,
	)
	assert.Error(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testAddFileUrlSuccess(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	fileName := "file_name"
	fileUrl := "file_url"
	expiredTimestamp := int64(23456789)

	mock.ExpectBegin()
	mock.ExpectExec("call fs_svc.add_file_url").
		WithArgs(projectId, fileType, fileName, fileUrl, expiredTimestamp).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err := repo.AddFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
		fileUrl,
		expiredTimestamp,
	)
	assert.NoError(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testGetFileUrlNotExisted(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	fileName := "file_name"

	rows := mock.NewRows([]string{"FileUrl"})
	mock.ExpectBegin()
	mock.ExpectQuery("call fs_svc.get_file_url").
		WithArgs(projectId, fileType, fileName).
		WillReturnRows(rows)
	mock.ExpectRollback()

	fileUrl, err := repo.GetFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
	)
	assert.Error(t, err)
	assert.Equal(t, fileUrl, "")

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testGetFileUrl(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	fileName := "file_name"

	rows := mock.NewRows([]string{"FileUrl"}).
		AddRow("file_temp_url_value")
	mock.ExpectBegin()
	mock.ExpectQuery("call fs_svc.get_file_url").
		WithArgs(projectId, fileType, fileName).
		WillReturnRows(rows)
	mock.ExpectCommit()

	url, err := repo.GetFileUrl(
		context.Background(),
		projectId,
		fileType,
		fileName,
	)
	assert.NoError(t, err)
	assert.NotNil(t, url)
	assert.Equal(t, url, "file_temp_url_value")

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testDeleteFileUrlUnSuccess(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	filePath := "form"
	fileName := "file_name"

	fn := filePath + "/" + fileName
	mock.ExpectBegin()
	mock.ExpectExec("call fs_svc.delete_file_url").
		WithArgs(projectId, fileType, fn).
		WillReturnError(errors.New("invalid project")).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectRollback()

	err := repo.DeleteFileUrl(
		context.Background(),
		projectId,
		fileType,
		filePath,
		fileName,
	)
	assert.Error(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func testDeleteFileUrlSuccess(t *testing.T, mock sqlmock.Sqlmock, repo port.FileStorageRepository) {
	projectId := "project_id"
	fileType := int8(1)
	filePath := "form"
	fileName := "file_name"

	fn := filePath + "/" + fileName
	mock.ExpectBegin()
	mock.ExpectExec("call fs_svc.delete_file_url").
		WithArgs(projectId, fileType, fn).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err := repo.DeleteFileUrl(
		context.Background(),
		projectId,
		fileType,
		filePath,
		fileName,
	)
	assert.NoError(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}
