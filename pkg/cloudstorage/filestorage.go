package cloudstorage

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/nfnt/resize"
)

type (
	reqRegisterTempUrl struct {
		UserId        int    `json:"userId"`
		RegionId      string `json:"regionId"`
		ProjectId     string `json:"projectId"`
		ContainerName string `json:"containerName"`
		Path          string `json:"path"`
		ExpiredTime   int64  `json:"expiredTime"`
	}

	respRegisterTempUrl struct {
		Code int    `json:"code"`
		Url  string `json:"data"`
	}

	reqGetTempUrls struct {
		UserId        int      `json:"userId"`
		RegionId      string   `json:"regionId"`
		ProjectId     string   `json:"projectId"`
		ContainerName string   `json:"containerName"`
		ExpiredTime   int64    `json:"timeExpire"`
		MethodType    string   `json:"methodType"`
		Objects       []string `json:"objects"`
	}

	respGetTempUrls struct {
		Code int               `json:"code"`
		Urls map[string]string `json:"data"`
	}

	reqDeleteFile struct {
		UserId        int    `json:"userId"`
		RegionId      string `json:"regionId"`
		ProjectId     string `json:"projectId"`
		ContainerName string `json:"containerName"`
		Path          string `json:"objectPath"`
	}
)

type FileStorage struct {
	userId    int
	regionId  string
	projectId string
	accessKey string
	checksum  string
	timestamp int64
}

func NewFileStorage(userId int, regionId, projectId, accessKey, checksum string, timestamp int64) *FileStorage {
	return &FileStorage{
		userId:    userId,
		regionId:  regionId,
		projectId: projectId,
		accessKey: accessKey,
		checksum:  checksum,
		timestamp: timestamp,
	}
}
func (p *FileStorage) ConvertImageToByte(img image.Image, format string) ([]byte, error) {
	buf := new(bytes.Buffer)
	var err error
	if format == "png" {
		err = png.Encode(buf, img)
		if err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	}

	err = jpeg.Encode(buf, img, nil)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (p *FileStorage) ResizeFile(fileContent []byte, limitSize int) ([]byte, error) {
	img, format, err := image.Decode(bytes.NewBuffer(fileContent))
	if err != nil {
		return nil, errors.New("the file size is larger than the allowed")
	}

	currentSize := len(fileContent)
	if currentSize <= limitSize {
		return fileContent, nil
	}

	var scale float32 = 1.0
	for currentSize > limitSize {
		scale -= 0.03
		newImg := resize.Resize(
			uint(float32(img.Bounds().Max.X)*scale),
			uint(float32(img.Bounds().Max.Y)*scale),
			img,
			resize.Lanczos3,
		)

		fileContent, err = p.ConvertImageToByte(newImg, format)
		if err != nil {
			return nil, err
		}
		currentSize = len(fileContent)
	}

	return fileContent, nil
}

func (p *FileStorage) RegisterTempUrlForPostFile(containerName, filePath, fileName string, expiredTime int64) (string, error) {
	tr := &http.Transport{
		MaxIdleConns:    10,
		IdleConnTimeout: 30 * time.Second,
	}
	client := http.Client{
		Transport: tr,
	}

	jsonBytes, err := json.Marshal(&reqRegisterTempUrl{
		UserId:        p.userId,
		RegionId:      p.regionId,
		ProjectId:     p.projectId,
		ContainerName: containerName,
		Path:          filePath,
		ExpiredTime:   expiredTime,
	})
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf("https://vos.vngcloud.vn/v1/directories/%s/tempurl", fileName),
		bytes.NewBuffer(jsonBytes),
	)
	if err != nil {
		return "", err
	}

	req.Header.Add("access_key", p.accessKey)
	req.Header.Add("checksum", p.checksum)
	req.Header.Add("timestamp", strconv.FormatInt(p.timestamp, 10))
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	body := resp.Body
	defer body.Close()
	if resp.StatusCode != http.StatusOK {
		return "", errors.New("invalid code")
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var respData respRegisterTempUrl
	err = json.Unmarshal(bodyBytes, &respData)
	if err != nil {
		return "", err
	}

	return respData.Url, err
}

func (p *FileStorage) PostFile(url string, contentReader io.Reader) error {
	tr := &http.Transport{
		MaxIdleConns:    10,
		IdleConnTimeout: 30 * time.Second,
	}
	client := http.Client{
		Transport: tr,
	}
	req, err := http.NewRequest("PUT", url, contentReader)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "text/plain")
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	body := resp.Body
	defer body.Close()
	if resp.StatusCode != http.StatusCreated {
		return errors.New("invalid code")
	}

	return nil
}

func (p *FileStorage) GetFileTempUrls(containerName string, fileNames []string, expiredTime int64) (map[string]string, error) {
	tr := &http.Transport{
		MaxIdleConns:    10,
		IdleConnTimeout: 30 * time.Second,
	}
	client := http.Client{
		Transport: tr,
	}

	jsonBytes, err := json.Marshal(&reqGetTempUrls{
		UserId:        p.userId,
		RegionId:      p.regionId,
		ProjectId:     p.projectId,
		ContainerName: containerName,
		ExpiredTime:   expiredTime,
		MethodType:    "GET",
		Objects:       fileNames,
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(
		"POST",
		"https://vos.vngcloud.vn/v1/objects/tempurl",
		bytes.NewBuffer(jsonBytes),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("access_key", p.accessKey)
	req.Header.Add("checksum", p.checksum)
	req.Header.Add("timestamp", strconv.FormatInt(p.timestamp, 10))
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	body := resp.Body
	defer body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("invalid code")
	}

	bodyBytes, err := io.ReadAll(body)
	if err != nil {
		return nil, err
	}

	var respData respGetTempUrls
	err = json.Unmarshal(bodyBytes, &respData)
	if err != nil {
		return nil, err
	}

	return respData.Urls, nil
}

func (p *FileStorage) GetFileUrl(cloud_id, containerName, fileName string) (string, error) {
	return fmt.Sprintf(
		"https://han01.vstorage.vngcloud.vn/v1/AUTH_{%s}/{%s}/{%s}",
		cloud_id,
		containerName,
		fileName,
	), nil
}

func (p *FileStorage) DeleteFile(containerName, filePath, fileName string) error {
	tr := &http.Transport{
		MaxIdleConns:    10,
		IdleConnTimeout: 30 * time.Second,
	}
	client := http.Client{
		Transport: tr,
	}

	jsonBytes, err := json.Marshal(&reqDeleteFile{
		UserId:        p.userId,
		RegionId:      p.regionId,
		ProjectId:     p.projectId,
		ContainerName: containerName,
		Path:          filePath,
	})
	if err != nil {
		return err
	}

	req, err := http.NewRequest(
		"DELETE",
		fmt.Sprintf("https://vos.vngcloud.vn/v1/objects/%s", fileName),
		bytes.NewBuffer(jsonBytes),
	)
	if err != nil {
		return err
	}

	req.Header.Add("access_key", p.accessKey)
	req.Header.Add("checksum", p.checksum)
	req.Header.Add("timestamp", strconv.FormatInt(p.timestamp, 10))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	body := req.Body
	defer body.Close()
	if resp.StatusCode != http.StatusAccepted {
		return errors.New("invalid code")
	}

	return nil
}
