package grpcserver

type Config struct {
	Hostname    string
	Name        string
	IP          string
	Port        int
	PrivateKey  string
	Certificate string
}
