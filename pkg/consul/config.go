package consul

type Config struct {
	Token           string
	Address         string
	Certificate     string
	HealthCheckIP   string
	HealthCheckPort int
}
