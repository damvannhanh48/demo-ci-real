package consul

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	consulConfig "github.com/goldeneye-inside/consul-connector-golang/config"
	"github.com/goldeneye-inside/consul-connector-golang/consul"
)

type ConsulClient interface {
	RegisterService(hostName, name, ip string, port int) error
}

func NewConsulClient(cfg *Config, certPath string) ConsulClient {
	connector := consul.NewConnector(&consulConfig.Config{
		Address:         cfg.Address,
		Token:           cfg.Token,
		Certificate:     cfg.Certificate,
		CertificatePath: certPath,
	})

	return &consulClient{
		cfg:       cfg,
		connector: connector,
	}
}

type consulClient struct {
	cfg       *Config
	connector consul.Connector
}

func (p *consulClient) RegisterService(hostName, name, ip string, port int) error {
	healthCheckURL := fmt.Sprintf("http://%s:%d/health-check", p.cfg.HealthCheckIP, p.cfg.HealthCheckPort)
	go p.startHealthCheckingServer(p.cfg.HealthCheckPort)
	return p.connector.RegisterService(hostName, name, ip, uint32(port), healthCheckURL)
}

func (p *consulClient) startHealthCheckingServer(port int) error {
	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"HEAD", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Type", "Access-Token"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
	}))
	router.Group("/").GET("/health-check", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, "ok")
	})
	return router.Run(fmt.Sprintf("0.0.0.0:%d", port))
}
