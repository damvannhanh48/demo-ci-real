package entity

type LogItem struct {
	LogType        LogType
	LoggerIdentity string
	Message        string
	Data           interface{}
	Error          error
}
