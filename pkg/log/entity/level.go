package entity

type Level int

const (
	DebugLevel Level = iota
	InfoLevel
	WarningLevel
	ErrorLevel
	FatalLevel
)

func GetLogLevels(ignoredLevels ...Level) []Level {
	levels := []Level{
		DebugLevel, InfoLevel, WarningLevel, ErrorLevel, FatalLevel,
	}
	if len(ignoredLevels) == 0 {
		return levels
	}

	var filterLevels []Level
	set := make(map[string]Level)
	for _, level := range ignoredLevels {
		set[level.String()] = level
	}
	for _, level := range levels {
		_, ok := set[level.String()]
		if !ok {
			filterLevels = append(filterLevels, level)
		}
	}
	return filterLevels
}

func GetLowerLogLevels(level Level, ignoredLevels ...Level) []Level {
	var lowerLevels []Level
	levels := GetLogLevels(ignoredLevels...)
	for _, lowerLevel := range levels {
		if lowerLevel < level {
			lowerLevels = append(lowerLevels, lowerLevel)
		}
	}
	return lowerLevels
}

func GetHigherLogLevels(level Level, ignoredLevels ...Level) []Level {
	var higherLevels []Level
	levels := GetLogLevels(ignoredLevels...)
	for _, higherLevel := range levels {
		if higherLevel >= level {
			higherLevels = append(higherLevels, higherLevel)
		}
	}
	return higherLevels
}

func (level Level) String() string {
	switch level {
	case InfoLevel:
		return "info"
	case WarningLevel:
		return "warn"
	case ErrorLevel:
		return "error"
	case FatalLevel:
		return "fatal"
	default:
		return "debug"
	}
}
