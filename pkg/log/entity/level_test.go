package entity

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogLevel(t *testing.T) {
	t.Run("getter_cases", execLogLevelGetterCases)
}

func execLogLevelGetterCases(t *testing.T) {
	testGetLogLevels(t)
	testGetLowerLogLevels(t)
	testGetHigherLogLevels(t)
	testLogLevelString(t)
}

func testGetLogLevels(t *testing.T) {
	levels := GetLogLevels()
	assert.Equal(t, 5, len(levels), "invalid number of levels")
	assert.Contains(t, levels, DebugLevel, "missing debug level")
	assert.Contains(t, levels, InfoLevel, "missing info level")
	assert.Contains(t, levels, WarningLevel, "missing warning level")
	assert.Contains(t, levels, ErrorLevel, "missing error level")
	assert.Contains(t, levels, FatalLevel, "missing fatal level")
}

func testGetLowerLogLevels(t *testing.T) {
	levels := GetLogLevels()
	numberLevels := len(levels)

	// Without ignore levels
	for _, level := range levels {
		lowerLevels := GetLowerLogLevels(level)
		for _, lowerLevel := range lowerLevels {
			assert.Less(t, lowerLevel, level, "")
		}
	}

	// With ignore levels
	for _, level := range levels {
		idx := 0
		ignoredLevels := make([]Level, 0, numberLevels)
		for idx < numberLevels {
			ignoredLevels = append(ignoredLevels, levels[idx])
			lowerLevels := GetLowerLogLevels(level, ignoredLevels...)
			for _, ignoredLevel := range ignoredLevels {
				assert.NotContains(t, lowerLevels, ignoredLevel)
			}
			for _, lowerLevel := range lowerLevels {
				assert.Less(t, lowerLevel, level, "")
			}
			idx++
		}
	}
}

func testGetHigherLogLevels(t *testing.T) {
	levels := GetLogLevels()
	numberLevels := len(levels)

	// Without ignore levels
	for _, level := range levels {
		higherLevels := GetHigherLogLevels(level)
		for _, higherLevel := range higherLevels {
			assert.GreaterOrEqual(t, higherLevel, level, "")
		}
	}

	// With ignore levels
	for _, level := range levels {
		idx := 0
		ignoredLevels := make([]Level, 0, numberLevels)
		for idx < numberLevels {
			ignoredLevels = append(ignoredLevels, levels[idx])
			higherLevels := GetHigherLogLevels(level, ignoredLevels...)
			for _, ignoredLevel := range ignoredLevels {
				assert.NotContains(t, higherLevels, ignoredLevel)
			}
			for _, higherLevel := range higherLevels {
				assert.GreaterOrEqual(t, higherLevel, level, "")
			}
			idx++
		}
	}
}

func testLogLevelString(t *testing.T) {
	levels := GetLogLevels()
	for _, level := range levels {
		switch level {
		case InfoLevel:
			assert.Equal(t, "info", level.String(), "invalid string")
		case WarningLevel:
			assert.Equal(t, "warn", level.String(), "invalid string")
		case ErrorLevel:
			assert.Equal(t, "error", level.String(), "invalid string")
		case FatalLevel:
			assert.Equal(t, "fatal", level.String(), "invalid string")
		case DebugLevel:
			assert.Equal(t, "debug", level.String(), "invalid string")
		}
	}
}
