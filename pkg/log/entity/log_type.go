package entity

type LogType int

const (
	AllLogType LogType = iota
	ConsoleLogType
	FileLogType
)
