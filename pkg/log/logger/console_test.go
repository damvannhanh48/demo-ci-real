package logger

import (
	"bytes"
	"encoding/json"
	"errors"
	"testing"

	"filestorage-service/pkg/log/entity"

	"github.com/stretchr/testify/assert"
)

type logResult struct {
	Level     string `json:"level"`
	Identity  string `json:"identity"`
	Data      string `json:"data"`
	Message   string `json:"message"`
	Timestamp string `json:"time"`
	Error     string `json:"error"`
}

type expectedLogResult struct {
	Level    entity.Level
	LogType  entity.LogType
	Identity string
	Message  string
	Data     string
	Error    error
}

func TestConsoleLog(t *testing.T) {
	t.Run("getter_cases", execConsoleLogGetterCases)
	t.Run("error_cases", execConsoleLogErrorCases)
	t.Run("success_cases", execConsoleLogSuccessCases)
}

func execConsoleLogGetterCases(t *testing.T) {
	ignoredLevel := entity.FatalLevel
	levels := entity.GetLogLevels(ignoredLevel)
	for _, level := range levels {
		expectedLogResult := buildExpectedLogResult(level, "identity")
		l := NewConsoleLogger(expectedLogResult.Identity, expectedLogResult.Level)
		assert.Equal(t, entity.ConsoleLogType, l.GetType(), "wrong type")
		assert.Equal(t, expectedLogResult.Identity, l.GetIdentity(), "wrong identity")
		assert.Equal(t, expectedLogResult.Level, l.GetLevel(), "wrong level")
	}
}

func execConsoleLogErrorCases(t *testing.T) {
	ignoredLevel := entity.FatalLevel
	levels := entity.GetLogLevels(ignoredLevel)
	for _, level := range levels {
		lowerLevels := entity.GetLowerLogLevels(level, ignoredLevel)
		for _, lowerLevel := range lowerLevels {
			testConsoleLogErrorCase(t, level, lowerLevel)
		}
	}
}

func execConsoleLogSuccessCases(t *testing.T) {
	ignoredLevel := entity.FatalLevel
	levels := entity.GetLogLevels(ignoredLevel)
	for _, level := range levels {
		higherLevels := entity.GetHigherLogLevels(level, ignoredLevel)
		for _, higherLevel := range higherLevels {
			testConsoleLogSuccessCase(t, higherLevel)
		}
	}
}

func testConsoleLogErrorCase(t *testing.T, logLevel entity.Level, errorLevel entity.Level) {
	expectedResult := buildExpectedLogResult(errorLevel, "identity")

	logBuffer := bytes.Buffer{}
	l := NewConsoleLogger(expectedResult.Identity, logLevel, &logBuffer)
	writeConsoleLog(
		l,
		expectedResult.Level,
		expectedResult.Message,
		expectedResult.Data,
		expectedResult.Error,
	)

	assert.Empty(t, logBuffer, "should be empty")
}

func testConsoleLogSuccessCase(t *testing.T, logLevel entity.Level) {
	expectedResult := buildExpectedLogResult(logLevel, "identity")

	logBuffer := bytes.Buffer{}
	l := NewConsoleLogger(expectedResult.Identity, expectedResult.Level, &logBuffer)

	writeConsoleLog(
		l,
		expectedResult.Level,
		expectedResult.Message,
		expectedResult.Data,
		expectedResult.Error,
	)
	validateLogResult(t, expectedResult, logBuffer.Bytes())
}

func writeConsoleLog(l Logger, level entity.Level, msg, data string, err error) {
	switch level {
	case entity.InfoLevel:
		l.Info(msg, data, err)
	case entity.ErrorLevel:
		l.Error(msg, data, err)
	case entity.FatalLevel:
		l.Fatal(msg, data, err)
	case entity.WarningLevel:
		l.Warn(msg, data, err)
	default:
		l.Debug(msg, data, err)
	}
}

func buildExpectedLogResult(logLevel entity.Level, identity string) *expectedLogResult {
	prefixLevel := logLevel.String() + "_"
	strIdentity := identity
	if len(identity) > 0 {
		strIdentity = prefixLevel + strIdentity
	}
	return &expectedLogResult{
		Level:    logLevel,
		Identity: strIdentity,
		Message:  prefixLevel + "message",
		Data:     prefixLevel + "data",
		Error:    errors.New(prefixLevel + "error"),
	}
}

func validateLogResult(t *testing.T, expectedResult *expectedLogResult, logResultBytes []byte) {
	var result logResult
	err := json.Unmarshal(logResultBytes, &result)
	assert.NoError(t, err, "parse log failed")
	if len(expectedResult.Identity) > 0 {
		assert.Equal(t, expectedResult.Identity, result.Identity, "wrong identity")
	}
	assert.Equal(t, expectedResult.Level.String(), result.Level, "wrong level")
	assert.Equal(t, expectedResult.Message, result.Message, "wrong message")
	assert.Equal(t, expectedResult.Data, result.Data, "wrong data")
	assert.Equal(t, expectedResult.Error.Error(), result.Error, "wrong error")
}
