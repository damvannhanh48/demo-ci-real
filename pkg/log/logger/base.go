package logger

import (
	"io"

	"filestorage-service/pkg/log/entity"

	"github.com/rs/zerolog"
)

type baseLogger struct {
	identity string
	logger   zerolog.Logger
}

func (p *baseLogger) GetLevel() entity.Level {
	return p.convertZeroLevel(p.logger.GetLevel())
}

func (p *baseLogger) GetIdentity() string {
	return p.identity
}

func (p *baseLogger) initialize(identity string, level entity.Level, writers ...io.Writer) {
	w := zerolog.MultiLevelWriter(writers...)
	event := zerolog.New(w).With().Timestamp()
	if len(identity) > 0 {
		event = event.Str("identity", identity)
	}
	p.logger = event.Logger()
	p.logger = p.logger.Level(p.convertLevel(level))
	p.identity = identity
}

func (p *baseLogger) writeLog(logEvent *zerolog.Event, msg string, data interface{}, err error) {
	if err == nil {
		logEvent.Interface("data", data).Msg(msg)
		return
	}
	logEvent.Interface("data", data).Err(err).Msg(msg)
}

func (p *baseLogger) convertLevel(level entity.Level) zerolog.Level {
	switch level {
	case entity.InfoLevel:
		return zerolog.InfoLevel
	case entity.WarningLevel:
		return zerolog.WarnLevel
	case entity.ErrorLevel:
		return zerolog.ErrorLevel
	case entity.FatalLevel:
		return zerolog.FatalLevel
	default:
		return zerolog.DebugLevel
	}
}

func (p *baseLogger) convertZeroLevel(level zerolog.Level) entity.Level {
	switch level {
	case zerolog.InfoLevel:
		return entity.InfoLevel
	case zerolog.WarnLevel:
		return entity.WarningLevel
	case zerolog.ErrorLevel:
		return entity.ErrorLevel
	case zerolog.FatalLevel:
		return entity.FatalLevel
	default:
		return entity.DebugLevel
	}
}
