package logger

import (
	"filestorage-service/pkg/log/entity"
)

type Logger interface {
	GetLevel() entity.Level
	GetType() entity.LogType
	GetIdentity() string // used to identify the logger when more loggers have the same type
	Debug(msg string, data interface{}, err error)
	Info(msg string, data interface{}, err error)
	Warn(msg string, data interface{}, err error)
	Error(msg string, data interface{}, err error)
	Fatal(msg string, data interface{}, err error)
}
