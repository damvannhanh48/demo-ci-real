package logger

import (
	"io"
	"os"

	"filestorage-service/pkg/log/entity"
)

/*
NewConsoleLogger inits a new instace of Console logger
Params:
	identity: used to identify the logger when more loggers have the same type (can be empty)
	level: log level (debug, info, warning, error, fatal)
	filters: used to filter or hook logs (implement zerolog.LevelWriter to filter logs)
*/
func NewConsoleLogger(identity string, level entity.Level, filters ...io.Writer) Logger {
	p := new(consoleLogger)
	filters = append(filters, os.Stdout)
	p.initialize(identity, level, filters...)
	return p
}

type consoleLogger struct {
	baseLogger
}

func (p *consoleLogger) GetType() entity.LogType {
	return entity.ConsoleLogType
}

func (p *consoleLogger) Debug(msg string, data interface{}, err error) {
	p.writeLog(p.logger.Debug(), msg, data, err)
}

func (p *consoleLogger) Info(msg string, data interface{}, err error) {
	p.writeLog(p.logger.Info(), msg, data, err)
}

func (p *consoleLogger) Warn(msg string, data interface{}, err error) {
	p.writeLog(p.logger.Warn(), msg, data, err)
}

func (p *consoleLogger) Error(msg string, data interface{}, err error) {
	p.writeLog(p.logger.Error(), msg, data, err)
}

func (p *consoleLogger) Fatal(msg string, data interface{}, err error) {
	p.writeLog(p.logger.Fatal(), msg, data, err)
}
