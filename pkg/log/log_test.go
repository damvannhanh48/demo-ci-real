package log

import (
	"bytes"
	"encoding/json"
	"errors"
	"testing"

	"filestorage-service/pkg/log/entity"
	"filestorage-service/pkg/log/logger"

	"github.com/stretchr/testify/assert"
)

type itemLoggerTesting struct {
	LogType  entity.LogType
	Identity string
	Channel  *bytes.Buffer
}

type logResult struct {
	Level     string `json:"level"`
	Identity  string `json:"identity"`
	Data      string `json:"data"`
	Message   string `json:"message"`
	Timestamp string `json:"time"`
	Error     string `json:"error"`
}

type expectedLogResult struct {
	Level    entity.Level
	LogType  entity.LogType
	Identity string
	Message  string
	Data     string
	Error    error
}

func TestLog(t *testing.T) {
	itemsTesting := []*itemLoggerTesting{
		&itemLoggerTesting{
			LogType:  entity.ConsoleLogType,
			Identity: "identity_a",
			Channel:  &bytes.Buffer{},
		},
		&itemLoggerTesting{
			LogType:  entity.ConsoleLogType,
			Identity: "identity_b",
			Channel:  &bytes.Buffer{},
		},
		&itemLoggerTesting{
			LogType:  entity.ConsoleLogType,
			Identity: "identity_c",
			Channel:  &bytes.Buffer{},
		},
	}
	t.Run("log_error_cases", func(t *testing.T) {
		execLogErrorCases(t, itemsTesting)
	})
	t.Run("log_success_cases", func(t *testing.T) {
		execLogSuccessCases(t, itemsTesting)
	})
}

func execLogErrorCases(t *testing.T, itemsTesting []*itemLoggerTesting) {
	ignoredLevel := entity.FatalLevel
	levels := entity.GetLogLevels(ignoredLevel)
	for _, level := range levels {
		loggers := buildLoggers(level, itemsTesting)
		if len(loggers) == 0 {
			assert.Fail(t, "missing log-type")
		}
		lowerLogLevels := entity.GetLowerLogLevels(level, ignoredLevel)
		for _, lowerLogLevel := range lowerLogLevels {
			l, err := New(loggers)
			assert.NoError(t, err, "should be success")

			for _, item := range itemsTesting {
				testLogErrorCase(t, l, entity.ConsoleLogType, lowerLogLevel, item.Identity, itemsTesting)
			}
			testLogErrorCase(t, l, entity.ConsoleLogType, lowerLogLevel, "", itemsTesting)

			for _, item := range itemsTesting {
				testLogErrorCase(t, l, entity.AllLogType, lowerLogLevel, item.Identity, itemsTesting)
			}
			testLogErrorCase(t, l, entity.AllLogType, lowerLogLevel, "", itemsTesting)
		}
	}
}

func execLogSuccessCases(t *testing.T, itemsTesting []*itemLoggerTesting) {
	ignoredLevel := entity.FatalLevel
	levels := entity.GetLogLevels(ignoredLevel)
	for _, level := range levels {
		loggers := buildLoggers(level, itemsTesting)
		if len(loggers) == 0 {
			assert.Fail(t, "missing log-type")
		}
		higherLogLevels := entity.GetHigherLogLevels(level, ignoredLevel)
		for _, higherLogLevel := range higherLogLevels {
			l, err := New(loggers)
			assert.NoError(t, err, "should be success")

			for _, item := range itemsTesting {
				testLogSuccessCase(t, l, entity.ConsoleLogType, higherLogLevel, item.Identity, itemsTesting)
			}
			testLogSuccessCase(t, l, entity.ConsoleLogType, higherLogLevel, "", itemsTesting)

			for _, item := range itemsTesting {
				testLogSuccessCase(t, l, entity.AllLogType, higherLogLevel, item.Identity, itemsTesting)
			}
			testLogSuccessCase(t, l, entity.AllLogType, higherLogLevel, "", itemsTesting)
		}
	}
}

func testLogErrorCase(t *testing.T, l Log, logType entity.LogType, logLevel entity.Level, identity string, itemsTesting []*itemLoggerTesting) {
	expectedLogType := logType
	expectedResult := buildExpectedLogResult(logLevel, identity)

	writeLog(
		l,
		expectedLogType,
		expectedResult.Level,
		expectedResult.Identity,
		expectedResult.Message,
		expectedResult.Data,
		expectedResult.Error,
	)
	defer func() {
		for _, item := range itemsTesting {
			item.Channel.Reset()
		}
	}()

	for _, item := range itemsTesting {
		assert.Empty(t, item.Channel.Bytes(), "should be empty")
	}
}

func testLogSuccessCase(t *testing.T, l Log, logType entity.LogType, logLevel entity.Level, identity string, itemsTesting []*itemLoggerTesting) {
	lenIdentity := len(identity)
	expectedLogType := logType
	expectedResult := buildExpectedLogResult(logLevel, identity)

	writeLog(
		l,
		expectedLogType,
		expectedResult.Level,
		expectedResult.Identity,
		expectedResult.Message,
		expectedResult.Data,
		expectedResult.Error,
	)
	defer func() {
		for _, item := range itemsTesting {
			item.Channel.Reset()
		}
	}()

	if expectedLogType == entity.AllLogType {
		if lenIdentity == 0 {
			// Validate all items
			for _, item := range itemsTesting {
				assert.NotEmpty(t, item.Channel.Bytes(), "should not be empty")
				validateLogResult(t, expectedResult, item.Channel.Bytes())
			}
			return
		}
		// Validate all items have the same identity (all log-types)
		for _, item := range itemsTesting {
			if item.Identity != expectedResult.Identity {
				assert.Empty(t, item.Channel.Bytes(), "should be empty")
				continue
			}
			assert.NotEmpty(t, item.Channel.Bytes(), "should not be empty")
			validateLogResult(t, expectedResult, item.Channel.Bytes())
		}
		return
	}

	if len(expectedResult.Identity) == 0 {
		// Validate all items have the same log-type (all identities)
		for _, item := range itemsTesting {
			if item.LogType != expectedLogType {
				assert.Empty(t, item.Channel.Bytes(), "should be empty")
				continue
			}
			assert.NotEmpty(t, item.Channel.Bytes(), "should not be empty")
			validateLogResult(t, expectedResult, item.Channel.Bytes())
		}
		return
	}

	// Validate all items have the same log-type and identity
	for _, item := range itemsTesting {
		if item.LogType != expectedLogType || item.Identity != expectedResult.Identity {
			assert.Empty(t, item.Channel.Bytes(), "should be empty")
			continue
		}
		assert.NotEmpty(t, item.Channel.Bytes(), "should not be empty")
		validateLogResult(t, expectedResult, item.Channel.Bytes())
	}
}

func buildLoggers(logLevel entity.Level, itemsTesting []*itemLoggerTesting) []logger.Logger {
	loggers := make([]logger.Logger, 0, len(itemsTesting))
	for _, item := range itemsTesting {
		if item.LogType != entity.ConsoleLogType {
			return []logger.Logger{}
		}
		loggers = append(loggers, logger.NewConsoleLogger(
			item.Identity, logLevel, item.Channel,
		))
	}
	return loggers
}

func writeLog(l Log, logType entity.LogType, logLevel entity.Level, identify, msg, data string, err error) {
	item := entity.LogItem{
		LogType:        logType,
		LoggerIdentity: identify,
		Message:        msg,
		Data:           data,
		Error:          err,
	}
	switch logLevel {
	case entity.InfoLevel:
		l.Info(item)
	case entity.ErrorLevel:
		l.Error(item)
	case entity.FatalLevel:
		l.Fatal(item)
	case entity.WarningLevel:
		l.Warn(item)
	default:
		l.Debug(item)
	}
}

func buildExpectedLogResult(logLevel entity.Level, identity string) *expectedLogResult {
	prefixLevel := logLevel.String() + "_"
	strIdentity := identity
	if len(identity) > 0 {
		strIdentity = prefixLevel + strIdentity
	}
	return &expectedLogResult{
		Level:    logLevel,
		Identity: strIdentity,
		Message:  prefixLevel + "message",
		Data:     prefixLevel + "data",
		Error:    errors.New(prefixLevel + "error"),
	}
}

func validateLogResult(t *testing.T, expectedResult *expectedLogResult, logResultBytes []byte) {
	var result logResult
	err := json.Unmarshal(logResultBytes, &result)
	assert.NoError(t, err, "parse log failed")
	if len(expectedResult.Identity) > 0 {
		assert.Equal(t, expectedResult.Identity, result.Identity, "wrong identity")
	}
	assert.Equal(t, expectedResult.Level.String(), result.Level, "wrong level")
	assert.Equal(t, expectedResult.Message, result.Message, "wrong message")
	assert.Equal(t, expectedResult.Data, result.Data, "wrong data")
	assert.Equal(t, expectedResult.Error.Error(), result.Error, "wrong error")
}
