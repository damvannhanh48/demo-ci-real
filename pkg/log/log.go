package log

import (
	"errors"

	"filestorage-service/pkg/log/entity"
	"filestorage-service/pkg/log/logger"
)

type LoggerSet map[entity.LogType][]logger.Logger

type Log interface {
	Debug(item entity.LogItem)
	Info(item entity.LogItem)
	Warn(item entity.LogItem)
	Error(item entity.LogItem)
	Fatal(item entity.LogItem)
}

func New(loggers []logger.Logger) (Log, error) {
	dict, err := buildLoggerSet(loggers)
	if err != nil {
		return nil, err
	}
	return &log{
		loggerSet: dict,
	}, nil
}

func buildLoggerSet(loggers []logger.Logger) (LoggerSet, error) {
	dict := make(LoggerSet)
	for _, itemLogger := range loggers {
		logType := itemLogger.GetType()
		items, ok := dict[logType]
		if !ok {
			items = make([]logger.Logger, 0)
		}
		for _, item := range items {
			if item.GetIdentity() == itemLogger.GetIdentity() {
				return nil, errors.New("duplicated logger")
			}
		}
		items = append(items, itemLogger)
		dict[logType] = items
	}
	return dict, nil
}

type log struct {
	loggerSet LoggerSet
}

func (p *log) Debug(item entity.LogItem) {
	loggers, err := p.buildLoggers(item.LogType, item.LoggerIdentity)
	if err != nil {
		return
	}
	for _, logger := range loggers {
		logger.Debug(item.Message, item.Data, item.Error)
	}
}

func (p *log) Info(item entity.LogItem) {
	loggers, err := p.buildLoggers(item.LogType, item.LoggerIdentity)
	if err != nil {
		return
	}
	for _, logger := range loggers {
		logger.Info(item.Message, item.Data, item.Error)
	}
}

func (p *log) Warn(item entity.LogItem) {
	loggers, err := p.buildLoggers(item.LogType, item.LoggerIdentity)
	if err != nil {
		return
	}
	for _, logger := range loggers {
		logger.Warn(item.Message, item.Data, item.Error)
	}
}

func (p *log) Error(item entity.LogItem) {
	loggers, err := p.buildLoggers(item.LogType, item.LoggerIdentity)
	if err != nil {
		return
	}
	for _, logger := range loggers {
		logger.Error(item.Message, item.Data, item.Error)
	}
}

func (p *log) Fatal(item entity.LogItem) {
	loggers, err := p.buildLoggers(item.LogType, item.LoggerIdentity)
	if err != nil {
		return
	}
	for _, logger := range loggers {
		logger.Fatal(item.Message, item.Data, item.Error)
	}
}

func (p *log) buildLoggers(logType entity.LogType, loggerIdentity string) ([]logger.Logger, error) {
	if logType != entity.AllLogType {
		return p.buildLoggersWith(logType, loggerIdentity)
	}

	lenIdentity := len(loggerIdentity)
	numberLoggers := 0
	for _, loggers := range p.loggerSet {
		if lenIdentity == 0 {
			numberLoggers += len(loggers)
			continue
		}
		for _, logger := range loggers {
			if logger.GetIdentity() == loggerIdentity {
				numberLoggers += 1
			}
		}
	}

	if numberLoggers == 0 {
		return nil, errors.New("not exists logger")
	}

	items := make([]logger.Logger, 0, numberLoggers)
	for _, loggers := range p.loggerSet {
		if lenIdentity == 0 {
			items = append(items, loggers...)
			continue
		}
		for _, logger := range loggers {
			if logger.GetIdentity() == loggerIdentity {
				items = append(items, logger)
			}
		}
	}
	return items, nil
}

func (p *log) buildLoggersWith(logType entity.LogType, loggerIdentity string) ([]logger.Logger, error) {
	numberLoggers := 0
	filterByIdentity := len(loggerIdentity) != 0
	for key, loggers := range p.loggerSet {
		if key != logType {
			continue
		}
		if filterByIdentity == false {
			numberLoggers += len(loggers)
			continue
		}
		for _, logger := range loggers {
			if logger.GetIdentity() == loggerIdentity {
				numberLoggers += 1
			}
		}
	}

	if numberLoggers == 0 {
		return nil, errors.New("not exists logger")
	}

	items := make([]logger.Logger, 0, numberLoggers)
	for key, loggers := range p.loggerSet {
		if key != logType {
			continue
		}
		if filterByIdentity == false {
			items = append(items, loggers...)
			continue
		}
		for _, logger := range loggers {
			if logger.GetIdentity() == loggerIdentity {
				items = append(items, logger)
			}
		}
	}
	return items, nil
}
